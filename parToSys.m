function [A,B,C,D] = parToSys(pars,Ts,prior)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
p1=prior.p1;
n=size(prior.A,1);
p2=n-p1;
p=prior.p;
q=size(prior.C,1)-p;
ss=[1,p1^2,p1*p2,p1*p,p1*q,p*p1,p*p2,p*q];
par_ss=@(i)pars(sum(ss(1:i))-1+(1:ss(i+1)));

A11=reshape(par_ss(1),[p1,p1]);
A12=reshape(par_ss(2),[p1,p2]);
K11=reshape(par_ss(3),[p1,p]);
K12=reshape(par_ss(4),[p1,q]);
C11=reshape(par_ss(5),[p,p1]);
C12=reshape(par_ss(6),[p,p2]);
Q12=reshape(par_ss(7),[p,q]);

A22=prior.A(p1+1:end,p1+1:end);
K22=prior.B(p1+1:end,p+1:end);
C22=prior.C(p+1:end,p1+1:end);
Q22=prior.Qeg(p+1:end,p+1:end);

D0=Q12/Q22;
A=[A11, A12-K12*C22-K11*D0*C22; ...
       zeros(n-p1,p1), A22-K22*C22];
B=[K12+K11*D0;...
       K22];
C=[C11, C12-D0*C22];
D=D0;
end