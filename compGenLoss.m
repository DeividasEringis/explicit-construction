function L = compGenLoss(A_hat,B_hat,C_hat,D_hat,gen)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

n=size(gen.A,1);
p=gen.p;

% We expect only stable systems
unstable=false;
[V_,D_]=eig(A_hat);
for i=1:n
    if (abs(D_(i,i))>1-1e-2)
        unstable=true;
        D_(i,i)=sign(D_(i,i))*(1-1e-2);
    end
end
if unstable
A_hat=real(V_*D_/V_);
end




C1=gen.C(1:p,:);C2=gen.C(p+1:end,:);
fe.A=[gen.A,zeros(n); B_hat*C2, A_hat];
fe.B=[gen.B;[zeros(n,p), B_hat]];
fe.C=[C1-D_hat*C2, -C_hat];
fe.D=[eye(p,p),-D_hat];

P=dlyap(fe.A,fe.B*gen.Qeg*fe.B'); % solve P=Ae*P*Ae'+ Ke*Qe*Ke'
% or in matlab notation solve: Ae*P*Ae'-P+Ke*Q*Ke'=0
L=trace(fe.C*P*fe.C'+fe.D*gen.Qeg*fe.D');
end