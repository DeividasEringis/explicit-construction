function f = PartitionSys(A,K,C,p,p1)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
f.A1=A(1:p1,1:p1);
f.A2=A(1:p1,p1+1:end);
f.A3=A(p1+1:end,p1+1:end);

f.K1=K(1:p1,1:p);
f.K2=K(1:p1,p+1:end);
f.K3=K(p1+1:end,p+1:end);

f.C1=C(1:p,1:p1);
f.C2=C(1:p,p1+1:end);
f.C3=C(p+1:end,p1+1:end);
end

