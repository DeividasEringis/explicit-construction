clc
clear
dec=2; % How many decimals in latex output
%% Start
% simple system


A=[1.08,-0.23;
    0.58,0.27];
B=[-0.56,-1.4;-0.56,-0.6];
C=[-0.25,2.25;1.24,-1.25];
D=[-0.14,-1;0,-1];
n=2; %number of states
p=1; % number of outputs y
p1=1; % order of y's subsystem

%% Step 1
P=dlyap(A,B*B'); %  (6.108a) E[x(t)x(t)']
fprintf('P=%s\n',latexMat(P,dec))
Cbar=C*P*A'+D*B'; % (6.108b) E[y(t)x(t+1)'] it was G in previous code
fprintf('\\bar{C}=%s\n',latexMat(Cbar,dec))

L0y=C*P*C'+D*D'; %  (6.108c) E[y(t)y(t)'], Lambda_0 in book
fprintf('\\Lambda_0=%s\n',latexMat(L0y,dec))

[PI,~,~]=idare(A',C',zeros(n,n),-L0y,-Cbar',eye(n));
fprintf('\\Pi=%s\n',latexMat(PI,dec))

Qe=L0y-C*PI*C'; % =Delta(PI)=E[e(t)e(t)']
Qe=(Qe+Qe')/2; % Qe should be symmetric, but due to numerical issues it might
% not be exactly symmetric, this will remove the small numerical assymetry
fprintf('Q_e=%s\n',latexMat(Qe,dec))


K=(Cbar'-A*PI*C')/Qe;
fprintf('K_g=%s\n',latexMat(K,dec))
%%

fprintf('\n generator: \n \\x(t+1)&=%s\\x(t)+%s\\e(t)\\\\ \n \\begin{bmatrix} \\y(t) \\\\ \\w(t) \\end{bmatrix} &= %s \\x(t)+\\e(t),\\quad Q=%s \n',latexMat(A,dec),latexMat(K,dec),latexMat(C,dec),latexMat(Qe,dec))

%% step 2 
%  get in block upper triangular form

[U,S,V]=svd(obsv(A,C(2,:)));

T=V(:,end:-1:1)';
fprintf('T=V^T=%s\n',latexMat(T,dec))
A_=T*A*T';
A_(2,1)=0; % just so small epsilon does not get dragged on
B_=T*K;
B_(2,1)=0;
C_=C*T';
C_(2,1)=0;
fprintf('\n generator in upper triangular form: \n \\x(t+1)&=%s\\x(t)+%s\\e(t)\\\\ \n \\begin{bmatrix} \\y(t) \\\\ \\w(t) \\end{bmatrix} &= %s \\x(t)+\\e(t)\n',latexMat(A_,dec),latexMat(B_,dec),latexMat(C_,dec))


%% Step 3
%  compute estimator

s=PartitionSys(A_,B_,C_,p,p1);
D0=Qe(1:p,p+1:end)/Qe(p+1:end,p+1:end);
A_opt=[s.A1, s.A2-s.K2*s.C3-s.K1*D0*s.C3; ...
       zeros(size(s.A3,1),size(s.A1,2)), s.A3-s.K3*s.C3];
B_opt=[s.K2+s.K1*D0;...
       s.K3];
C_opt=[s.C1, s.C2-D0*s.C3];
D_opt=D0;
fprintf('\n estimator: \n \\hat{\\x}(t+1)&=%s\\hat{\\x}(t)+%s\\w(t)\\\\ \n \\hat{\\y}(t) &= %s \\hat{\\x}(t)+%s\\w(t)\n',latexMat(A_opt,dec),latexMat(B_opt,dec),latexMat(C_opt,dec),latexMat(D_opt,dec))

%% Parameterisation

Ag=sym(round(A_,dec));
Ag(1,2)=sym('theta')
fprintf('\n parameterised generator: \n \\hat{\\x}(t+1)&=%s\\hat{\\x}(t)+%s\\w(t)\\\\ \n \\hat{\\y}(t) &= %s \\hat{\\x}(t)+%s\\w(t)\n',latexMat(Ag,dec),latexMat(B_opt,dec),latexMat(C_opt,dec),latexMat(D_opt,dec))

s=PartitionSys(Ag,B_,C_,p,p1);
D0=Qe(1:p,p+1:end)/Qe(p+1:end,p+1:end);
A_opt_par=[s.A1, s.A2-s.K2*s.C3-s.K1*D0*s.C3; ...
       zeros(size(s.A3,1),size(s.A1,2)), s.A3-s.K3*s.C3];
B_opt_par=[s.K2+s.K1*D0;...
       s.K3];
C_opt_par=[s.C1, s.C2-D0*s.C3];
D_opt_par=D0;
fprintf('\n parameterised estimator: \n \\hat{\\x}(t+1|\\theta)&=%s\\hat{\\x}(t|\\theta)+%s\\w(t)\\\\ \n \\hat{\\y}(t|\\theta) &= %s \\hat{\\x}(t|\\theta)+%s\\w(t)\n',latexMat(A_opt_par,dec),latexMat(B_opt_par,dec),latexMat(C_opt_par,dec),latexMat(D_opt_par,dec))




function s = latexMat(A,dec)
    if isa(A,'double')
        s=latex(sym(round(A,dec)));
    else
        s=latex(A);
    end

    s= regexprep(s,'(\\left\(\\begin{array}{)[c]+[}]','\\begin{bmatrix}');
    
    s = replace(s,'\end{array}\right)','\end{bmatrix}');
    s = regexprep(s,'(\d+[.]\d+?)0+','$1');
end