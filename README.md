# Explicit construction of the minimum error variance estimator for stochastic LTI-ss systems
Matlab code used for the examples in the paper:
Eringis, Deividas, et al. "Optimal Prediction of Unmeasured Output from Measurable Outputs In LTI Systems." arXiv preprint arXiv:2109.02384 (2021).

## Computational Example
Matlab script "compExampleSteps.m" defines the initial system, does all the computational steps and outputs latex code which was directly used in the paper, section 5 "Computational Example"

## System identification Example
Run Matlab Script "HighOrder_par_Example.m"
which will use parallel processing to generate many datasets and perform identification
will save progress to a file, which you can open in another matlab script "plotStats.m".
which will open the file generate figures and output a table similar to the one in the paper.
