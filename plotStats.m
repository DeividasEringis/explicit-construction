clc
% close all
clear
saveFile='StatsMIMO_2022_11_18_01_00_32.mat';
load(saveFile)
upPrc=99; %use 99 to ignore strangely large loss
names={'Case 2.1 know w, est. predictor','Case 1.1 know nothing, est. predictor','Case 1.2 know nothing, est. generator','Case 2.2 know w, est. generator','Case 0 know full system'};

target_Ns=[150,1000];


if any(contains(fieldnames(Stats),'optBFRval'))==false

n=size(gen.A,1);
p=gen.p;
q=size(gen.C,1)-p;

s=PartitionSys(gen.A,gen.B,gen.C,gen.p,gen.p1);
rank(obsv(s.A3,s.C3))

D0=gen.Qeg(1:p,p+1:end)/gen.Qeg(p+1:end,p+1:end);
A_opt=[s.A1, s.A2-s.K2*s.C3-s.K1*D0*s.C3; ...
       zeros(size(s.A3,1),size(s.A1,2)), s.A3-s.K3*s.C3];
B_opt=[s.K2+s.K1*D0;...
       s.K3];
C_opt=[s.C1, s.C2-D0*s.C3];
D_opt=D0;

Nmax=max(target_Ns);

x=zeros(n,1);
for i=1:1e3
   v=mvnrnd(zeros(p+q,1),gen.Qeg)';
   x=gen.A*x+gen.B*v;
end


x=[x,zeros(n,Nmax-1)];
y=zeros(p,Nmax);
w=zeros(q,Nmax);
x_hat=zeros(n,Nmax);
y_opt=zeros(p,Nmax);

optBFRval=nan(2e3,2,p);
optVAFval=nan(2e3,2,p);

for traj=1:2000
for i=1:Nmax
   v=mvnrnd(zeros(p+q,1),gen.Qeg)';
   x(:,i+1)=gen.A*x(:,i)+gen.B*v;
   y(:,i)=gen.C(1:p,:)*x(:,i)+v(1:p);
   w(:,i)=gen.C(p+1:end,:)*x(:,i)+v(p+1:end);
   
   x_hat(:,i+1)=A_opt*x_hat(:,i)+B_opt*w(:,i);
   y_opt(:,i)=C_opt*x_hat(:,i)+D_opt*w(:,i);
end
for i=1:numel(target_Ns)
    N=target_Ns(i);
    optBFRval(traj,i,:)=(max(1-sqrt(sum((y(:,1:N)-y_opt(:,1:N)).^2,2)./sum((y(:,1:N)-mean(y(:,1:N))).^2,2)),0)*100);
    optVAFval(traj,i,:)=(max(1-var((y(:,1:N)-y_opt(:,1:N))')./var(y(:,1:N)'),0)*100);
end

end

Stats.optBFRval=optBFRval;
Stats.optVAFval=optVAFval;
save(saveFile,'L_opt','Ns',"Stats","gen");
end
%%


figure('Name','Training/Validation MSE');
    subplot(2,1,1)
    minMSE=inf;
    for i=1:4
        MSE=Stats.MSE{i};
        vec=zeros(1,numel(Ns));
        for N_i=1:numel(Ns)
        mse=filterY(MSE(:,N_i));
        vec(N_i)=mean(mse);
        end
        minMSE=min(minMSE,min(vec(Ns>150)));
        semilogx(Ns,vec,'-*');
        hold on
    end
    yline(L_opt)
    hold off
%     legend(names,'location','southeast')
grid on
    ylabel('Average Training MSE')
    xlim([150,Ns(end)])
    xticks([150,gca().XTick])
    ylim([0.95*minMSE,gca().Children(3).YData(8)])
    xlabel('N')
    subplot(2,1,2)
    minL=inf;
    for i=1:4
        L=Stats.L{i};
        vec=zeros(1,numel(Ns));
        for N_i=1:numel(Ns)
            l=filterY(L(:,N_i));
        vec(N_i)=mean(l);
        end
        minL=min(minL,min(vec(Ns>150)));
        semilogx(Ns,vec,'-*');
        hold on
    end
    yline(L_opt)
    hold off
    grid on
    legend(names)
    ylabel('Average Validation MSE')
    xlim([150,Ns(end)])
    xticks([150,gca().XTick])
    ylim([0.95*L_opt,gca().Children(3).YData(8)])
    xlabel('N')
    plotStyleFormat
    f=gcf;
    f.Position(3:4)=[5.2187    4.2188];
exportgraphics(gcf,'Average.eps','ContentType','vector');
exportgraphics(gcf,'Average.pdf','ContentType','vector');


%%


figure('Name','Validation Histograms');
subplot(5,1,1)
hist_N=109;

[~,hist_N_i]=min(abs(hist_N-Ns));
allL=cell2mat(cellfun(@(L) L(isnan(L(:,hist_N_i))==false,hist_N_i)',Stats.L,'UniformOutput',false));
allL=allL(allL<=prctile(allL,upPrc));

[hc,hb]=histcounts(allL);
binWidth=2*iqr(allL)*numel(allL)^(-1/3);

hb=L_opt:((hb(2)-hb(1))/4):hb(end);
pd=cell(4,1);
h=cell(1,4);
maxL=cell(4,1);

for i=1:4
    subplot(5,1,i)
    L=Stats.L{i};

    L=filterY(L(:,hist_N_i));
    
    maxL{i}=max(L);
    t=mean(L(L<3))+10*std(L(L<3));
    h{i}=histogram(L(L<t),'Normalization','probability');
    if max(L)>h{i}.BinEdges(end)
    h{i}=histogram(L,[h{i}.BinEdges,max(L)],'Normalization','probability');
    end
    hold on
    xline(mean(L))
    hold off
    pd{i} = fitdist((L-L_opt),'lognormal'); % 'ev' , Gamma gev Kernel

    ylabel('Normalised Bin Counts')
    xlabel('Validation Loss')
    title(names{i});
end

subplot(5,1,5)
for i=1:4
    x=[linspace(L_opt,3,1000),linspace(3,max(cell2mat(maxL)),100)];
    p=plot(x,pd{i}.pdf((x-L_opt)));
    hold on
end
hold off
xlabel('x')
ylabel('P(L<x)')
legend(names(1:4))

figure('Name','fitdist')
for i=1:4
    x=[linspace(L_opt,3,1000),linspace(3,max(cell2mat(maxL)),100)];
    p=plot(x,pd{i}.pdf((x-L_opt)));
    hold on
end
hold off
xlabel('x')
ylabel('P(L<x)')
legend(names(1:4))

%% Report
target_Ns=[150,1000];
for i=1:numel(target_Ns)
    [~,hist_N_i]=min(abs(target_Ns(i)-Ns));
    target_Ns(i)=Ns(hist_N_i);
end
target_Ns=unique(target_Ns);
Report=nan(1,5);
rownames={};
boldFcns={};
prefixCols={''};
rownames{1}='\multicolumn{2}{r|}{Total Parameters}';
boldFcns{1}=@(x) false;
Report(1,:)=([96 , 156 , 150 , 90,0]);
prefixCols{end+1,1}='';
suffixEntries={''};

for Ns_i=1:numel(target_Ns)
    [~,hist_N_i]=min(abs(target_Ns(Ns_i)-Ns));
    allL=cell2mat(cellfun(@(L) L(isnan(L(:,hist_N_i))==false,hist_N_i)',Stats.L,'UniformOutput',false));
    L_target=prctile(allL,5);

    temprowMeanL=zeros(1,4);
    temprowMeanMSE=zeros(1,4);
    
    for i=1:4
        L=Stats.L{i};
        L=filterY(L(:,hist_N_i));
        temprowMeanL(i)=mean(L);
        MSE=Stats.MSE{i};
        MSE=MSE(isnan(MSE(:,hist_N_i))==false,hist_N_i);
        temprowMeanMSE(i)=mean(MSE);
    end
    temprowMeanL(5)=L_opt;

    

    Report(end+1,:)=([temprowMeanMSE nan]);
    rownames{end+1}='Training MSE';
    boldFcns{end+1}=@(v,x) v==min(temprowMeanMSE);
    prefixCols{end+1}=sprintf('\\hline \\multicolumn{1}{r|}{\\multirow{4}{*}{\\centering $N=%i$}}',Ns(hist_N_i));
    suffixEntries{end+1,1}='';


    Report(end+1,:)=(temprowMeanL);
    rownames{end+1}='Validation MSE';
    boldFcns{end+1}=@(v,x) v==min(temprowMeanL(1:4));
    prefixCols{end+1}='\multicolumn{1}{r|}{}';
    suffixEntries{end+1,1}='';
    
    for p_=1:gen.p
    Report(end+1,1:4)=cellfun(@(BFRval) mean(BFRval(:,hist_N_i,p_)),Stats.BFRval);
    Report(end,5)=mean(Stats.optBFRval(:,Ns_i,p_),'omitnan');
    Report(end,:)=round(Report(end,:),1);
    rownames{end+1}=sprintf('Validation $BFR_%i$',p_);
    boldFcns{end+1}=@(v,x) v==max(Report(end,1:4));
    prefixCols{end+1}='\multicolumn{1}{r|}{}';
    suffixEntries{end+1,1}='%';
    end


    Report(end+1,1:4)=cellfun(@(BFRval) mean(mean(BFRval(:,hist_N_i,:),3)),Stats.BFRval);
    Report(end,5)=mean(mean(Stats.optBFRval(:,Ns_i,:),3,'omitnan'));
    Report(end,:)=round(Report(end,:),1);
    rownames{end+1}='Validation $\bar{BFR}$';
    boldFcns{end+1}=@(v,x) v==max(Report(end,1:4));
    prefixCols{end+1}='\multicolumn{1}{r|}{}';
    suffixEntries{end+1,1}='%';


    for p_=1:gen.p
    Report(end+1,1:4)=cellfun(@(VAFval) mean(VAFval(:,hist_N_i,p_)),Stats.VAFval);
    Report(end,5)=mean(Stats.optVAFval(:,Ns_i,p_),'omitnan');
    Report(end,:)=round(Report(end,:),1);
    rownames{end+1}=sprintf('Validation $VAF_%i$',p_);
    boldFcns{end+1}=@(v,x) v==max(Report(end,1:4));
    prefixCols{end+1}='\multicolumn{1}{r|}{}';
    suffixEntries{end+1,1}='%';
    end

    Report(end+1,1:4)=cellfun(@(VAFval) mean(mean(VAFval(:,hist_N_i,:),3)),Stats.VAFval);
    Report(end,5)=mean(mean(Stats.optVAFval(:,Ns_i,:),3,'omitnan'));
    Report(end,:)=round(Report(end,:),1);
    rownames{end+1}='Validation $\bar{VAF}$';
    boldFcns{end+1}=@(v,x) v==max(Report(end,1:4));
    prefixCols{end+1}='\multicolumn{1}{r|}{}';
    suffixEntries{end+1,1}='%';


end

reorder=[5,2,3,1,4];
t = array2table(Report(:,reorder),'VariableNames',names(reorder))










%%
function y =filterY(x)

x=x(isnan(x)==false);
y=x;
end