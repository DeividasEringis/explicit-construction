clear all
close all
rng(161)
% continueFile='StatsMIMO_2022_11_08_01_37_21'; % set to '' to start new file
continueFile='';

if strcmp(continueFile,'')
p1=4;
p2=6;
n=p1+p2;
p=3;
q=2;


Ag=[0.183670430000000	0.175265170000000	-0.00915016000000000	0.0683573900000000	-0.0741884600000000	0.186304120000000	-0.177297510000000	-0.233442730000000	-0.305253390000000	-0.131385950000000
-0.0978121000000000	0.0319831800000000	0.0411444500000000	0.00459383000000000	0.199329970000000	-0.188381230000000	-0.322065740000000	0.0211386100000000	0.0919488200000000	0.279804290000000
0.0781606100000000	-0.155307880000000	-0.182089140000000	0.0688385500000000	-0.209715600000000	-0.0716583100000000	-0.175874010000000	0.405102320000000	0.367714460000000	0.0985614800000000
-0.0643587800000000	0.111590540000000	-0.367006600000000	-0.245585930000000	0.0483650900000000	0.0592380000000000	0.110873450000000	-0.120439730000000	0.201972240000000	-0.0938443900000000
0	0	0	0	-0.169533180000000	-0.185855910000000	0.128498910000000	-0.530269710000000	0.438164290000000	-0.0559703300000000
0	0	0	0	0.523461120000000	-0.0757281000000000	0.192338680000000	0.0638457800000000	-0.155439830000000	0.439147070000000
0	0	0	0	-0.00374511000000000	-0.0656333600000000	0.162046360000000	-0.263015830000000	-0.108332150000000	-0.0324588100000000
0	0	0	0	-0.00347468000000000	-0.341274120000000	-0.126100640000000	-0.371946060000000	0.494055360000000	0.367239500000000
0	0	0	0	0.00367779000000000	0.0508010100000000	-0.139806840000000	0.143248080000000	0.700540650000000	0.588620770000000
0	0	0	0	0.00861790000000000	0.0210447600000000	-0.272404720000000	-0.0992563900000000	-0.322968810000000	-0.0338007700000000];

Kg=[-0.282404420000000	0.210394200000000	0.364951280000000	-0.324629360000000	-0.109521060000000
0.155642210000000	-0.220947890000000	0.0140421500000000	-0.282687900000000	0.598582890000000
-0.255581730000000	-0.0547277700000000	0.441007950000000	-0.121183980000000	0.119956990000000
-0.333583400000000	-0.0770320800000000	0.0393528900000000	0.333889750000000	0.0615291800000000
0	0	0	-0.139032690000000	0.0716761000000000
0	0	0	-0.298231150000000	-0.110313800000000
0	0	0	-0.193266520000000	-0.313740780000000
0	0	0	-0.374274170000000	-0.234060700000000
0	0	0	0.0820135700000000	0.275525950000000
0	0	0	0.0328680300000000	0.0586703500000000];

Cg=[0.340904560000000	-0.102020950000000	0.0252217000000000	-0.141226530000000	0.0517740500000000	-0.359976990000000	-0.127915730000000	-0.243778210000000	-0.423489560000000	-0.297037370000000
-0.132422200000000	-0.0974193100000000	-0.178342510000000	0.536231390000000	-0.263543550000000	-0.191576220000000	0.353917500000000	0.401060390000000	0.170124910000000	-0.0931612100000000
0.178169480000000	0.00640933000000000	0.182082580000000	-0.166612410000000	0.0184769100000000	-0.102009530000000	-0.256008910000000	0.268494040000000	0.101712250000000	-0.437394250000000
0	0	0	0	-0.0107035200000000	-0.0194294800000000	0.0405275700000000	0.0761557000000000	-0.405287520000000	0.811667180000000
0	0	0	0	0.00242379000000000	0.0773497700000000	0.0612127000000000	-0.347533390000000	0.0895633600000000	0.274891290000000];

Qeg=[1.14524433260134	0.283476689903965	-0.258389784802146	0.0338268453290823	0.221611768507062
0.283476689903965	0.236984393264262	-0.243596017999296	0.161324741532103	0.103316483127571
-0.258389784802146	-0.243596017999296	0.463489499152352	-0.0985220483823605	-0.284477114718446
0.0338268453290823	0.161324741532103	-0.0985220483823605	0.170387589770391	-0.00702993952013566
0.221611768507062	0.103316483127571	-0.284477114718446	-0.00702993952013566	0.223026928784132];


gen.A=Ag;
gen.B=Kg;
gen.C=Cg;
gen.Qeg=Qeg;
gen.p=p;
gen.p1=p1;


end
%%
Nmax=1e3;
Nmin=5e1;
Ns=unique(floor(logspace(log10(Nmin),log10(Nmax),20)));
% Ns=unique([Ns,(floor(logspace(log10(1e3),log10(Nmax),6)))]);
Ns=sort(Ns);



k_lim=500;
SearchMethod='gna'; % 'auto','gn','gna','lm','grad'
data_traj=2e3;
initIter=1e3; %initial iterations( when generating data) to not have transients
d = datetime(now,'ConvertFrom','datenum');
saveFileName=sprintf('StatsMIMO_%s',string(d,'yyyy_MM_dd_hh_mm_ss'));



if strcmp(continueFile,'')==false
    saveFileName=continueFile;
    load(saveFileName);
    p1=gen.p1;
    p2=size(gen.A,1)-p1;
    n=p1+p2;
    p=gen.p;
    q=size(gen.C,1)-p;
    Ag=gen.A;
    Kg=gen.B;
    Cg=gen.C;
    Qeg=gen.Qeg;
end

hist_N=n^2+n*q+p*n+p*q;
[~,hist_N_i]=min(abs(hist_N-Ns));
hist_N_i=hist_N_i+1;
hist_N=Ns(hist_N_i);


s=PartitionSys(Ag,Kg,Cg,p,p1);
rank(obsv(s.A3,s.C3))




D0=Qeg(1:p,p+1:end)/Qeg(p+1:end,p+1:end);
A_opt=[s.A1, s.A2-s.K2*s.C3-s.K1*D0*s.C3; ...
       zeros(size(s.A3,1),size(s.A1,2)), s.A3-s.K3*s.C3];
B_opt=[s.K2+s.K1*D0;...
       s.K3];
C_opt=[s.C1, s.C2-D0*s.C3];
D_opt=D0;

gen.A=Ag;
gen.B=Kg;
gen.C=Cg;
gen.Qeg=Qeg;
gen.p=p;
gen.p1=p1;
L_opt=compGenLoss(A_opt,B_opt,C_opt,D_opt,gen)



%%
x=zeros(n,1);
for i=1:initIter
   v=mvnrnd(zeros(p+q,1),Qeg)';
   x=Ag*x+Kg*v;
end


x=[x,zeros(n,Nmax-1)];
y=zeros(p,Nmax);
w=zeros(q,Nmax);
x_hat=zeros(n,Nmax);
y_hat=zeros(p,Nmax);
for i=1:Nmax
   v=mvnrnd(zeros(p+q,1),Qeg)';
   x(:,i+1)=Ag*x(:,i)+Kg*v;
   y(:,i)=Cg(1:p,:)*x(:,i)+v(1:p);
   w(:,i)=Cg(p+1:end,:)*x(:,i)+v(p+1:end);
   
   x_hat(:,i+1)=A_opt*x_hat(:,i)+B_opt*w(:,i);
   y_hat(:,i)=C_opt*x_hat(:,i)+D_opt*w(:,i);
end
cov([y;y_hat]')
err_cov=cov(y'-y_hat');
MSE_opt=cumsum(sum((y-y_hat).^2,1))./(1:Nmax);
MSE_opt=MSE_opt(Nmin:end);
% loglog(1:300,MSE_opt(1:300))
%% plots


figure
for i=1:p
subplot(p+1,1,i)
plot(y(i,:),'DisplayName','y')
hold on
% plot(w','DisplayName','w')
plot(y_hat(i,:),'DisplayName','y_hat')
hold off
title(sprintf('y(%i) and y_{hat}(%i)',i,i))
end

subplot(p+1,1,p+1)
plot(y(1,:)-y_hat(1,:),'DisplayName','err(1)')
title('y(1)-\hat{y}(1)','Interpreter','Latex')
f=gcf;
pos1=f.Position;
legend

figure
subplot(2,1,1)
plot(x(1,:),'DisplayName','x')
hold on
plot(x_hat(1,:),'DisplayName','x_hat')
hold off
legend
subplot(2,1,2)
plot(x(2,:),'DisplayName','x')
hold on
plot(x_hat(2,:),'DisplayName','x_hat')
hold off
legend
f=gcf;
f.Position(1)=f.Position(1)+pos1(3);
%%
figure
plot(y(1,:),'DisplayName','y')
hold on
% plot(w','DisplayName','w')
plot(y_hat(1,:),'DisplayName','y_{hat}')
hold off
legend('$y(t)$','$\hat{y}(t)$','Interpreter','latex')
plotStyleFormat
h.Position([3:4]) = [5,3]; % resize figure
exportgraphics(gcf,'pred.pdf','ContentType','vector');

% end %for sys= ?
%%


if (strcmp(continueFile,''))
dataCounts=zeros(4,numel(Ns));
Stats.L={nan(data_traj,numel(Ns)),nan(data_traj,numel(Ns)),nan(data_traj,numel(Ns)),nan(data_traj,numel(Ns))};
Stats.MSE={nan(data_traj,numel(Ns)),nan(data_traj,numel(Ns)),nan(data_traj,numel(Ns)),nan(data_traj,numel(Ns))};
Stats.BFR={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.VAF={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.BFRopt={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.VAFopt={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.BFRval={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.VAFval={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.BFRoptval={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};
Stats.VAFoptval={nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p),nan(data_traj,numel(Ns),p)};

MSE_know_w=zeros(1,numel(Ns));
L_know_w=zeros(1,numel(Ns));

MSE_know_nothing=zeros(1,numel(Ns));
L_know_nothing=zeros(1,numel(Ns));

MSE_gen=zeros(1,numel(Ns));
L_gen=zeros(1,numel(Ns));

MSE_gen_know_w=zeros(1,numel(Ns));
L_gen_know_w=zeros(1,numel(Ns));
else
    dataCounts=cell2mat(cellfun(@(L) sum(isnan(L)==false),Stats.L,'UniformOutput',false)');
    MSE_know_w=mean(Stats.MSE{1},'omitnan');
    L_know_w=mean(Stats.L{1},'omitnan');
        
    L_know_nothing=mean(Stats.L{2},'omitnan');
    MSE_know_nothing=mean(Stats.MSE{2},'omitnan');

    L_gen=mean(Stats.L{3},'omitnan');
    MSE_gen=mean(Stats.MSE{3},'omitnan');

    L_gen_know_w=mean(Stats.L{4},'omitnan');
    MSE_gen_know_w=mean(Stats.MSE{4},'omitnan');

end

    figure
    subplot(2,1,1)
    
    plMSE_know_nothing=loglog(Ns,MSE_know_nothing);
    hold on
    plMSE_know_w=loglog(Ns,MSE_know_w);
    plMSE_gen=loglog(Ns,MSE_gen);
    plMSE_gen_know_w=loglog(Ns,MSE_gen_know_w);
    plMSE_opt=loglog(Nmin:Nmax,MSE_opt);
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('Empirical MSE')
    
    subplot(2,1,2)
    
    plL_know_nothing=loglog(Ns,L_know_nothing);
    hold on
    plL_know_w=loglog(Ns,L_know_w);
    plL_gen=loglog(Ns,L_gen);
    plL_gen_know_w=loglog(Ns,L_gen_know_w);
    yline(L_opt);
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('Generalised Loss')

    figure
    subplot(4,2,1)
    for i=1:4
        plBFR{i}=semilogx(Ns,mean(mean(Stats.BFR{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average BFR')
    
    subplot(4,2,2)
    for i=1:4
        plVAF{i}=semilogx(Ns,mean(mean(Stats.VAF{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average VAF')
    subplot(4,2,3)
    for i=1:4
        plBFRopt{i}=semilogx(Ns,mean(mean(Stats.BFRopt{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average BFR of y_{opt}')
        subplot(4,2,4)
    for i=1:4
        plVAFopt{i}=semilogx(Ns,mean(mean(Stats.VAFopt{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average VAF of y_{opt}')


    subplot(4,2,5)
    for i=1:4
        plBFRval{i}=semilogx(Ns,mean(mean(Stats.BFRval{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average val BFR')
    
    subplot(4,2,6)
    for i=1:4
        plVAFval{i}=semilogx(Ns,mean(mean(Stats.VAFval{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average val VAF')
    subplot(4,2,7)
    for i=1:4
        plBFRoptval{i}=semilogx(Ns,mean(mean(Stats.BFRoptval{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average val BFR of y_{opt}')
        subplot(4,2,8)
    for i=1:4
        plVAFoptval{i}=semilogx(Ns,mean(mean(Stats.VAFoptval{i},3,'omitnan'),1,'omitnan'));
        hold on
    end
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator','know everything')
    title('average val VAF of y_{opt}')


    figure
    [~,hist_N_i]=min(abs(hist_N-Ns));
    pl_hist{1}=histogram([1,1],'DisplayStyle','stairs');
    hold on
    pl_hist{2}=histogram([1,1],'DisplayStyle','stairs');
    pl_hist{3}=histogram([1,1],'DisplayStyle','stairs');
    pl_hist{4}=histogram([1,1],'DisplayStyle','stairs');
    hold off
    legend('know nothing, est. predictor','know w, est. predictor','know nothing, est. generator','know w, est. generator')
    title(sprintf('Histogram of Loss at N=%i',hist_N))

progressbar('data','N_i');

ThreadInfo=cell(0,1);
N_threads=20;%50;%numel(Ns);%10;
for data_i=(max(max(dataCounts))+1):data_traj
x=zeros(n,1);
for i=1:initIter
   v=mvnrnd(zeros(p+q,1),Qeg)';
   x=Ag*x+Kg*v;
end


x=[x,zeros(n,Nmax-1)];
y=zeros(p,Nmax);
w=zeros(q,Nmax);
x_hat=zeros(n,Nmax);
y_opt=zeros(p,Nmax);
for i=1:2*Nmax
   v=mvnrnd(zeros(p+q,1),Qeg)';
   x(:,i+1)=Ag*x(:,i)+Kg*v;
   y(:,i)=Cg(1:p,:)*x(:,i)+v(1:p);
   w(:,i)=Cg(p+1:end,:)*x(:,i)+v(p+1:end);
   
   x_hat(:,i+1)=A_opt*x_hat(:,i)+B_opt*w(:,i);
   y_opt(:,i)=C_opt*x_hat(:,i)+D_opt*w(:,i);
end
err_cov=cov(y(:,1:Nmax)'-y_opt(:,1:Nmax)');
MSE_opt_=cumsum(sum((y(:,1:Nmax)-y_opt(:,1:Nmax)).^2,1))./(1:Nmax);
MSE_opt_=MSE_opt_(Nmin:end);
MSE_opt=((data_i-1)*MSE_opt+MSE_opt_)/data_i;
plMSE_opt.YData=MSE_opt;
drawnow


for N_i=1:numel(Ns)

    th=numel(ThreadInfo)+1;
    ThreadInfo{th}.data_i=data_i;
    ThreadInfo{th}.N_i=N_i;
    ThreadInfo{th}.N=Ns(N_i);
    ThreadInfo{th}.data=iddata(y(:,1:ThreadInfo{th}.N)',w(:,1:ThreadInfo{th}.N)');
    ThreadInfo{th}.y_opt=y_opt(:,1:Ns(N_i));
    ThreadInfo{th}.Autdata=iddata([y(:,1:ThreadInfo{th}.N)',w(:,1:ThreadInfo{th}.N)'],[y(:,1:ThreadInfo{th}.N)',w(:,1:ThreadInfo{th}.N)']);
    
    ThreadInfo{th}.y_opt_val=y_opt(:,Nmax:Nmax+Ns(N_i));
    ThreadInfo{th}.w_val=w(:,Nmax:Nmax+Ns(N_i));
    ThreadInfo{th}.y_val=y(:,Nmax:Nmax+Ns(N_i));
    
    ThreadInfo{th}.L=cell(4,1);
    ThreadInfo{th}.MSE=cell(4,1);
    ThreadInfo{th}.BFR=nan(4,p);
    ThreadInfo{th}.VAF=nan(4,p);
    ThreadInfo{th}.BFRopt=nan(4,p);
    ThreadInfo{th}.VAFopt=nan(4,p);
    ThreadInfo{th}.BFRval=nan(4,p);
    ThreadInfo{th}.VAFval=nan(4,p);
    ThreadInfo{th}.BFRoptval=nan(4,p);
    ThreadInfo{th}.VAFoptval=nan(4,p);

    % for N_i=1:numel(Ns)
    if (numel(ThreadInfo)==N_threads || (N_i==numel(Ns)&&data_i==data_traj))
    ThreadInfo=ThreadInfo(randperm(numel(ThreadInfo)));
    parfor th=1:numel(ThreadInfo)
    %     progressbar(data_i/data_traj,N_i/numel(Ns))
        N=ThreadInfo{th}.N;
        fprintf('%i: N_i=%i, N=%i\n',ThreadInfo{th}.data_i,ThreadInfo{th}.N_i,N)
    data=ThreadInfo{th}.data;
    w_th=get(data,'InputData')';
    y_th=get(data,'OutputData')';
    % init_sys=idss(A_hat,K_hat,C_hat,D0,randn(2,1),randn(2,1));
    
    y_opt=ThreadInfo{th}.y_opt;
    
    y_opt_val=ThreadInfo{th}.y_opt_val;
    w_val=ThreadInfo{th}.w_val;
    y_val=ThreadInfo{th}.y_val;
    
    %% Know w fully
    init_pars=reshape(Ag(1:p1,:),1,[]);
    init_pars=[init_pars,reshape(Kg(1:p1,:),1,[])];
    init_pars=[init_pars,reshape(Cg(1:p,:),1,[])];
    init_pars=[init_pars,reshape(Qeg(1:p,p+1:end),1,[])];
    
    init_sys=idgrey(@parToSys,init_pars,'d',{gen},1);
    try
    
    
    opt=greyestOptions('SearchMethod',SearchMethod,'InitialState','zero','EnforceStability',true,'EstimateCovariance',false);
    opt.SearchOptions.MaxIterations=k_lim;
    fit_sys=pem(data,init_sys,opt);
    A_hat=fit_sys.A;
    B_hat=fit_sys.B;
    C_hat=fit_sys.C;
    D_hat=fit_sys.D;
    
    
    ThreadInfo{th}.L{1}=compGenLoss(A_hat,B_hat,C_hat,D_hat,gen);
    
    y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_th(:,i);
       x_hat=A_hat*x_hat+B_hat*w_th(:,i);
    end
    ThreadInfo{th}.MSE{1}=mean(sum((y_th(:,1:N)-y_hat).^2,1));
    for i=1:p
        ThreadInfo{th}.BFR(1,i)=max(1-sqrt(sum((y_th(i,1:N)-y_hat(i,:)).^2)/sum((y_th(i,1:N)-mean(y_th(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAF(1,i)=max(1-var(y_th(i,1:N)-y_hat(i,:))/var(y_th(i,1:N)),0)*100;

        ThreadInfo{th}.BFRopt(1,i)=max(1-sqrt(sum((y_opt(i,1:N)-y_hat(i,:)).^2)/sum((y_opt(i,1:N)-mean(y_opt(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFopt(1,i)=max(1-var(y_opt(i,1:N)-y_hat(i,:))/var(y_opt(i,1:N)),0)*100;
    end
    y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_val(:,i);
       x_hat=A_hat*x_hat+B_hat*w_val(:,i);
    end
    for i=1:p
        ThreadInfo{th}.BFRval(1,i)=max(1-sqrt(sum((y_val(i,1:N)-y_hat(i,:)).^2)/sum((y_val(i,1:N)-mean(y_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFval(1,i)=max(1-var(y_val(i,1:N)-y_hat(i,:))/var(y_val(i,1:N)),0)*100;

        ThreadInfo{th}.BFRoptval(1,i)=max(1-sqrt(sum((y_opt_val(i,1:N)-y_hat(i,:)).^2)/sum((y_opt_val(i,1:N)-mean(y_opt_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFoptval(1,i)=max(1-var(y_opt_val(i,1:N)-y_hat(i,:))/var(y_opt_val(i,1:N)),0)*100;
    end

    catch ME
        fprintf([ME.identifier,'\n'])
    end
    %% Know nothing

    try

    init_sys=idss(A_opt,B_opt,C_opt,D_opt);
    opt=ssestOptions('SearchMethod',SearchMethod,'InitialState','zero','EnforceStability',true,'EstimateCovariance',false);
    fit_sys=pem(data,init_sys,opt);

    A_hat=fit_sys.A;
    B_hat=fit_sys.B;
    C_hat=fit_sys.C;
    D_hat=fit_sys.D;
    
    ThreadInfo{th}.L{2}=compGenLoss(A_hat,B_hat,C_hat,D_hat,gen);
    
    y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_th(:,i);
       x_hat=A_hat*x_hat+B_hat*w_th(:,i);
    end
    ThreadInfo{th}.MSE{2}=mean(sum((y_th(:,1:N)-y_hat).^2,1));
    for i=1:p
    ThreadInfo{th}.BFR(2,i)=max(1-sqrt(sum((y_th(i,1:N)-y_hat(i,:)).^2)/sum((y_th(i,1:N)-mean(y_th(i,1:N))).^2)),0)*100;
    ThreadInfo{th}.VAF(2,i)=max(1-var(y_th(i,1:N)-y_hat(i,:))/var(y_th(i,1:N)),0)*100;
            ThreadInfo{th}.BFRopt(2,i)=max(1-sqrt(sum((y_opt(i,1:N)-y_hat(i,:)).^2)/sum((y_opt(i,1:N)-mean(y_opt(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFopt(2,i)=max(1-var(y_opt(i,1:N)-y_hat(i,:))/var(y_opt(i,1:N)),0)*100;
    end
        y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_val(:,i);
       x_hat=A_hat*x_hat+B_hat*w_val(:,i);
    end
    for i=1:p
        ThreadInfo{th}.BFRval(2,i)=max(1-sqrt(sum((y_val(i,1:N)-y_hat(i,:)).^2)/sum((y_val(i,1:N)-mean(y_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFval(2,i)=max(1-var(y_val(i,1:N)-y_hat(i,:))/var(y_val(i,1:N)),0)*100;

        ThreadInfo{th}.BFRoptval(2,i)=max(1-sqrt(sum((y_opt_val(i,1:N)-y_hat(i,:)).^2)/sum((y_opt_val(i,1:N)-mean(y_opt_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFoptval(2,i)=max(1-var(y_opt_val(i,1:N)-y_hat(i,:))/var(y_opt_val(i,1:N)),0)*100;
    end
    catch ME
        fprintf([ME.identifier,'\n'])
    end
    %% Know nothing, estimate generator
    
    data=ThreadInfo{th}.Autdata;
    try
    init_sys=idss(Ag,[],Cg,[],Kg);
    init_sys.Structure.A.Free(p1+1:end,1:p1)=false;
    init_sys.Structure.K.Free(p1+1:end,1:p)=false;
    init_sys.Structure.C.Free(p+1:end,1:p1)=false;
    
    opt=ssestOptions('SearchMethod',SearchMethod,'InitialState','estimate','EnforceStability',true,'EstimateCovariance',false);
    opt.SearchOptions.MaxIterations=k_lim;

    fit_sys=pem(iddata(get(data,'OutputData')),init_sys,opt);


    eg_hat=zeros(p+q,N);
    x_hat=zeros(n,1);
    yw=get(data,'OutputData')';
    for i=1:N 
       eg_hat(:,i)=yw(:,i)-fit_sys.C*x_hat;
       x_hat=(fit_sys.A-fit_sys.K*fit_sys.C)*x_hat+fit_sys.K*yw(:,i);
    end
    Qeg_hat=cov(eg_hat');
    
    Ag_hat=fit_sys.A;
    Kg_hat=fit_sys.K;
    Cg_hat=fit_sys.C;
    s = PartitionSys(Ag_hat,Kg_hat,Cg_hat,p,p1);
    
    D0_hat=Qeg_hat(1:p,p+1:end)/Qeg_hat(p+1:end,p+1:end);
    A_hat=[s.A1, s.A2-s.K2*s.C3-s.K1*D0_hat*s.C3; ...
           zeros(size(s.A3,1),size(s.A1,2)), s.A3-s.K3*s.C3];
    B_hat=[s.K2+s.K1*D0_hat;...
           s.K3];
    C_hat=[s.C1, s.C2-D0_hat*s.C3];
    D_hat=D0_hat;
    
    
    
    ThreadInfo{th}.L{3}=compGenLoss(A_hat,B_hat,C_hat,D_hat,gen);
    
    y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_th(:,i);
       x_hat=A_hat*x_hat+B_hat*w_th(:,i);
    end
    ThreadInfo{th}.MSE{3}=mean(sum((y_th(:,1:N)-y_hat).^2,1));
    for i=1:p
    ThreadInfo{th}.BFR(3,i)=max(1-sqrt(sum((y_th(i,1:N)-y_hat(i,:)).^2)/sum((y_th(i,1:N)-mean(y_th(i,1:N))).^2)),0)*100;
    ThreadInfo{th}.VAF(3,i)=max(1-var(y_th(i,1:N)-y_hat(i,:))/var(y_th(i,1:N)),0)*100;
            ThreadInfo{th}.BFRopt(3,i)=max(1-sqrt(sum((y_opt(i,1:N)-y_hat(i,:)).^2)/sum((y_opt(i,1:N)-mean(y_opt(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFopt(3,i)=max(1-var(y_opt(i,1:N)-y_hat(i,:))/var(y_opt(i,1:N)),0)*100;
    end
        y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_val(:,i);
       x_hat=A_hat*x_hat+B_hat*w_val(:,i);
    end
    for i=1:p
        ThreadInfo{th}.BFRval(3,i)=max(1-sqrt(sum((y_val(i,1:N)-y_hat(i,:)).^2)/sum((y_val(i,1:N)-mean(y_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFval(3,i)=max(1-var(y_val(i,1:N)-y_hat(i,:))/var(y_val(i,1:N)),0)*100;

        ThreadInfo{th}.BFRoptval(3,i)=max(1-sqrt(sum((y_opt_val(i,1:N)-y_hat(i,:)).^2)/sum((y_opt_val(i,1:N)-mean(y_opt_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFoptval(3,i)=max(1-var(y_opt_val(i,1:N)-y_hat(i,:))/var(y_opt_val(i,1:N)),0)*100;
    end
    catch ME
        fprintf([ME.identifier,'\n'])
    end
    %% Know w, estimate generator
    
    
    init_pars=reshape(Ag(1:p1,:),1,[]);
    init_pars=[init_pars,reshape(Kg(1:p1,:),1,[])];
    init_pars=[init_pars,reshape(Cg(1:p,:),1,[])];
    try
    init_sys=idgrey(@knowWGenPred,init_pars,'d',{gen},1);
    opt=greyestOptions('SearchMethod',SearchMethod,'InitialState','zero','EnforceStability',true,'EstimateCovariance',false);
    opt.SearchOptions.MaxIterations=k_lim;
    fit_sys=pem(data,init_sys,opt);


    eg_hat=zeros(p+q,N);
    x_hat=zeros(n,1);
    yw=get(data,'OutputData')';
    for i=1:N 
       eg_hat(:,i)=yw(:,i)-fit_sys.C*x_hat;
       x_hat=fit_sys.A*x_hat+fit_sys.B*yw(:,i);
    end
    Qeg_hat=cov(eg_hat');

%     Ag_hat=fit_sys.A;
    Ag_hat=fit_sys.A+fit_sys.B*fit_sys.C;
    Kg_hat=fit_sys.B;
    Cg_hat=fit_sys.C;
    s = PartitionSys(Ag_hat,Kg_hat,Cg_hat,p,p1);
    
    D0_hat=Qeg_hat(1:p,p+1:end)/Qeg_hat(p+1:end,p+1:end);
    A_hat=[s.A1, s.A2-s.K2*s.C3-s.K1*D0_hat*s.C3; ...
           zeros(size(s.A3,1),size(s.A1,2)), s.A3-s.K3*s.C3];
    B_hat=[s.K2+s.K1*D0_hat;...
           s.K3];
    C_hat=[s.C1, s.C2-D0_hat*s.C3];
    D_hat=D0_hat;
    
    ThreadInfo{th}.L{4}=compGenLoss(A_hat,B_hat,C_hat,D_hat,gen);
    
    y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_th(:,i);
       x_hat=A_hat*x_hat+B_hat*w_th(:,i);
    end
    ThreadInfo{th}.MSE{4}=mean(sum((y_th(:,1:N)-y_hat).^2,1));
    for i=1:p
    ThreadInfo{th}.BFR(4,i)=max(1-sqrt(sum((y_th(i,1:N)-y_hat(i,:)).^2)/sum((y_th(i,1:N)-mean(y_th(i,1:N))).^2)),0)*100;
    ThreadInfo{th}.VAF(4,i)=max(1-var(y_th(i,1:N)-y_hat(i,:))/var(y_th(i,1:N)),0)*100;
            ThreadInfo{th}.BFRopt(4,i)=max(1-sqrt(sum((y_opt(i,1:N)-y_hat(i,:)).^2)/sum((y_opt(i,1:N)-mean(y_opt(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFopt(4,i)=max(1-var(y_opt(i,1:N)-y_hat(i,:))/var(y_opt(i,1:N)),0)*100;
    end
        y_hat=zeros(p,N);
    x_hat=zeros(n,1);
    for i=1:N 
       y_hat(:,i)=C_hat*x_hat+D_hat*w_val(:,i);
       x_hat=A_hat*x_hat+B_hat*w_val(:,i);
    end
    for i=1:p
        ThreadInfo{th}.BFRval(4,i)=max(1-sqrt(sum((y_val(i,1:N)-y_hat(i,:)).^2)/sum((y_val(i,1:N)-mean(y_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFval(4,i)=max(1-var(y_val(i,1:N)-y_hat(i,:))/var(y_val(i,1:N)),0)*100;

        ThreadInfo{th}.BFRoptval(4,i)=max(1-sqrt(sum((y_opt_val(i,1:N)-y_hat(i,:)).^2)/sum((y_opt_val(i,1:N)-mean(y_opt_val(i,1:N))).^2)),0)*100;
        ThreadInfo{th}.VAFoptval(4,i)=max(1-var(y_opt_val(i,1:N)-y_hat(i,:))/var(y_opt_val(i,1:N)),0)*100;
    end
    catch ME
        fprintf([ME.identifier,'\n'])
    end
    
    end
    
    for th=1:numel(ThreadInfo)
       
        N_i_th=ThreadInfo{th}.N_i;
        for i=1:4
            if isempty(ThreadInfo{th}.L{i})==false
                dataCounts(i,N_i_th)=dataCounts(i,N_i_th)+1;
                Stats.L{i}(ThreadInfo{th}.data_i,N_i_th)=ThreadInfo{th}.L{i};
                Stats.MSE{i}(ThreadInfo{th}.data_i,N_i_th)=ThreadInfo{th}.MSE{i};
                Stats.BFR{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.BFR(i,:);
                Stats.VAF{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.VAF(i,:);
                Stats.BFRopt{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.BFRopt(i,:);
                Stats.VAFopt{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.VAFopt(i,:);
                Stats.BFRval{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.BFRval(i,:);
                Stats.VAFval{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.VAFval(i,:);
                Stats.BFRoptval{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.BFRoptval(i,:);
                Stats.VAFoptval{i}(ThreadInfo{th}.data_i,N_i_th,:)=ThreadInfo{th}.VAFoptval(i,:);
                
            end
        end
        if isempty(ThreadInfo{th}.L{1})==false
            L_know_w(N_i_th)=          ((dataCounts(1,N_i_th)-1)*L_know_w(N_i_th)       +ThreadInfo{th}.L{1})/dataCounts(1,N_i_th);
            MSE_know_w(N_i_th)=        ((dataCounts(1,N_i_th)-1)*MSE_know_w(N_i_th)       +ThreadInfo{th}.MSE{1})/dataCounts(1,N_i_th);
        end
        if isempty(ThreadInfo{th}.L{2})==false
            L_know_nothing(N_i_th)=    ((dataCounts(2,N_i_th)-1)*L_know_nothing(N_i_th) +ThreadInfo{th}.L{2})/dataCounts(2,N_i_th);
            MSE_know_nothing(N_i_th)=  ((dataCounts(2,N_i_th)-1)*MSE_know_nothing(N_i_th) +ThreadInfo{th}.MSE{2})/dataCounts(2,N_i_th);
        end
        if isempty(ThreadInfo{th}.L{3})==false
            L_gen(N_i_th)=             ((dataCounts(3,N_i_th)-1)*L_gen(N_i_th)          +ThreadInfo{th}.L{3})/dataCounts(3,N_i_th);
            MSE_gen(N_i_th)=           ((dataCounts(3,N_i_th)-1)*MSE_gen(N_i_th)          +ThreadInfo{th}.MSE{3})/dataCounts(3,N_i_th);
            
        end
        if isempty(ThreadInfo{th}.L{4})==false
            L_gen_know_w(N_i_th)=      ((dataCounts(4,N_i_th)-1)*L_gen_know_w(N_i_th)   +ThreadInfo{th}.L{4})/dataCounts(4,N_i_th);
            MSE_gen_know_w(N_i_th)=    ((dataCounts(4,N_i_th)-1)*MSE_gen_know_w(N_i_th)   +ThreadInfo{th}.MSE{4})/dataCounts(4,N_i_th);
        end
        
        if (N_i_th==hist_N_i)
            for i=1:4
                pl_hist{i}.Data=Stats.L{i}(:,hist_N_i);
            end
        end
        
    end
    plL_know_w.YData=L_know_w;
    plMSE_know_w.YData=MSE_know_w;
    plL_know_nothing.YData=L_know_nothing;
    plMSE_know_nothing.YData=MSE_know_nothing;
    plL_gen.YData=L_gen;
    plMSE_gen.YData=MSE_gen;
    plL_gen_know_w.YData=L_gen_know_w;
    plMSE_gen_know_w.YData=MSE_gen_know_w;

    for i=1:4
        plBFR{i}.YData=mean(mean(Stats.BFR{i},3,'omitnan'),1,'omitnan');

        plVAF{i}.YData=mean(mean(Stats.VAF{i},3,'omitnan'),1,'omitnan');

        plBFRopt{i}.YData=mean(mean(Stats.BFRopt{i},3,'omitnan'),1,'omitnan');

        plVAFopt{i}.YData=mean(mean(Stats.VAFopt{i},3,'omitnan'),1,'omitnan');

        plBFRval{i}.YData=mean(mean(Stats.BFRval{i},3,'omitnan'),1,'omitnan');

        plVAFval{i}.YData=mean(mean(Stats.VAFval{i},3,'omitnan'),1,'omitnan');

        plBFRoptval{i}.YData=mean(mean(Stats.BFRoptval{i},3,'omitnan'),1,'omitnan');

        plVAFoptval{i}.YData=mean(mean(Stats.VAFoptval{i},3,'omitnan'),1,'omitnan');
    end

    drawnow
    save(saveFileName,'Stats','Ns','L_opt','gen')
    
    progressbar(data_i/data_traj,N_i/numel(Ns))
    ThreadInfo=cell(0,1);
    end
end


end

%%
function [A,B,C,D]=knowNothingPred(pars,Ts,prior)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

n=size(prior.A,1);
p=prior.p;
q=size(prior.C,1)-p;

A=reshape(pars(1:n*n),[n,n]);
pars=pars(n*n+1:end);
B=reshape(pars(1:n*q),[n,q]);
pars=pars(n*q+1:end);
C=reshape(pars(1:n*p),[p,n]);
pars=pars(n*p+1:end);
D=reshape(pars,[p,q]);
end

function [A,B,C,D]=knowNothingGenPred(pars,Ts,prior)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

n=size(prior.A,1);
p=prior.p;
q=size(prior.C,1)-p;
m=p+q;

Ag=reshape(pars(1:n*n),[n,n]);
pars=pars(n*n+1:end);
Kg=reshape(pars(1:n*m),[n,m]);
pars=pars(n*m+1:end);
Cg=reshape(pars(1:n*m),[m,n]);


A=Ag-Kg*Cg;
B=Kg;
C=Cg;
D=zeros(p+q,p+q);
end

function [A,B,C,D]=knowWGenPred(pars,Ts,prior)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

p1=prior.p1;
n=size(prior.A,1);
p2=n-p1;
p=prior.p;
q=size(prior.C,1)-p;
ss=[1,p1^2,p1*p2,p1*p,p1*q,p*p1,p*p2,p*q];
par_ss=@(i)pars(sum(ss(1:i))-1+(1:ss(i+1)));

A11=reshape(par_ss(1),[p1,p1]);
A12=reshape(par_ss(2),[p1,p2]);
K11=reshape(par_ss(3),[p1,p]);
K12=reshape(par_ss(4),[p1,q]);
C11=reshape(par_ss(5),[p,p1]);
C12=reshape(par_ss(6),[p,p2]);
% Q12=reshape(par_ss(7),[p,q]);

A22=prior.A(p1+1:end,p1+1:end);
K22=prior.B(p1+1:end,p+1:end);
C22=prior.C(p+1:end,p1+1:end);
% Q22=prior.Qeg(p+1:end,p+1:end);

Ag=[A11, A12; ...
   zeros(n-p1,p1), A22];
Kg=[K11,K12;...
    zeros(n-p1,p), K22];
Cg=[C11, C12;
    zeros(q,p1),C22];
A=Ag-Kg*Cg;
B=Kg;
C=Cg;
D=zeros(p+q,p+q);

end